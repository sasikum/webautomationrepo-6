
package com.tatahealth.EMR.pages.AppointmentDashboard;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;



public class AllSlotsPage {
	
	//*[@id="appointment-container"]/div[4]/ul/li[1]/div[2]/div[4]/div[3]   -  Plus button
	//*[@id="appointment-container"]/div[4]/ul/li[1]/div[2]/div[1]-  Slot Available
	
	public int getAvailableSlot(WebDriver driver, ExtentTest logger) 
	{
		int n = 4;
		while(n<200) {
			String path = "//*[@id='appointment-container']/div[4]/ul/li["+n+"]/div[2]/div[1]";
			WebElement element =  null;
			try {
				element = driver.findElement(By.xpath(path));
			}catch(Exception e) {
				n++;
				continue;
			}	
			
			if(element.isDisplayed()&&element.getText().trim().equalsIgnoreCase("Slot Available")){
				break;
			}else {
				n++;
			}
		}
		return n;	
	}
	
	public String getSlotId(int n, WebDriver driver,ExtentTest logger) {
		String csselector = "#appointment-container > div.rightbar > ul > li:nth-child("+n+") > div.slotSpecialCase > div.timediv > div.plus";
		String element = Web_GeneralFunctions.findElementbySelector(csselector,driver,logger).getAttribute("id").substring(4);
		return element;
	}
	
	
	public String getTimeforSelectedSlot(int n, WebDriver driver,ExtentTest logger) {
		String csselector = "#appointment-container > div.rightbar > ul > li:nth-child("+n+") > div.slotSpecialCase > div.timediv > div.slottime";
		String element = Web_GeneralFunctions.findElementbySelector(csselector,driver,logger).getText();
		return element;
	}
	
	
	public String getAMPMforSelectedSlot(int n, WebDriver driver,ExtentTest logger) {
		String csselector = "#appointment-container > div.rightbar > ul > li:nth-child("+n+") > div.slotSpecialCase > div.timediv > div.ampm";
		String element = Web_GeneralFunctions.findElementbySelector(csselector,driver,logger).getText();
		return element;
	}
	
	
	public boolean CheckSelectedSlotVisibility(int n, WebDriver driver,ExtentTest logger) {
		String csselector = "#appointment-container > div.rightbar > ul > li:nth-child("+n+") > div.slotSpecialCase > div.timediv > div.slottime";
		boolean element = Web_GeneralFunctions.findElementbySelector(csselector,driver,logger).isDisplayed();
		return element;
	}
	
	
	
	public WebElement getPlusIconforAvailableSlot(String slotId, WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#plus"+slotId,driver,logger);
		return element;
	}
	
	
	public WebElement getSearchBoxinAvailableSlot(String slotId, WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#searchpatient"+slotId,driver,logger);
		return element;
	}
	
	public WebElement getFirstAvailableConsumer(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#patientsearchresult > li:nth-child(1) > div.topspace.pull-right.greentext > a:nth-child(1)",driver,logger);
		return element;
	}
	
	
	public WebElement setReasonforVisit(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#reasonDoctorVisitRegister",driver,logger);
		return element;
	}
	
	public WebElement getBookAppointmentSubmitBtn(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#submitOnce",driver,logger);
		return element;
	}
	
	
	public WebElement getPrevMonthBtn(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector(".datepicker-days .prev > .glyphicon",driver,logger);
		return element;
	}
	
	
	public WebElement getNextMonthBtn(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector(".datepicker-days .next > .glyphicon",driver,logger);
		return element;
	}
	
	public WebElement getDatetoClick(int n, int week, WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("tr:nth-child("+n+") > .day:nth-child("+(week)+")",driver,logger);
		return element;
	}
	
	public WebElement getCurrentDate(WebDriver driver,ExtentTest logger) {
		WebElement element = driver.findElement(By.className("calactive"));
		return element;
	}
	// below old Xpath is for AppointmentDropDown arrow link in the drop down

	public WebElement getAppointmentDropDown(int n, WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#appointment-container > div.rightbar > ul > li:nth-child("+n+
				") > div.muiltiple > div.pull-left.padtop10.col-lg-7 > div.pull-left.padleftright.text-center.btnpadtop.relative "
				+ "> a > img",driver,logger);
		return element;
	}
	
	
	public int getAppointmentDropDownNum(WebDriver driver,ExtentTest logger) {
		List<WebElement> myElements = driver.findElements(By.xpath(" //a[@aria-haspopup='true' and @class='dropdown-toggle']"));

		int num= myElements.size();
 		return num;
	}
	
	
	  public WebElement getAppointmentDropDown1(int num, WebDriver driver,ExtentTest logger) 
	  { WebElement element =
	  Web_GeneralFunctions.findElementbyXPath("( //a[@aria-haspopup='true' and @class='dropdown-toggle'])["+num+"]", driver, logger); 
	  return element; }
	 
	
	
	
	public WebElement getBlockedSlot(int n, WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#appointment-container > div.rightbar > ul > li:nth-child("+n+") > div:nth-child(3) > p",driver,logger);
		return element;
	}
	
	public WebElement getCheckinToConsultfromDropdown(int n, WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("",driver,logger);
		return element;
	}
	
	
	/*
	 * public WebElement getReschedulefromDropdown(int n, WebDriver
	 * driver,ExtentTest logger) { WebElement element = Web_GeneralFunctions.
	 * findElementbySelector("#appointment-container > div.rightbar > ul > li:nth-child("
	 * +n+
	 * ") > div.muiltiple > div.pull-left.padtop10.col-lg-7 > div.pull-left.padleftright.text-center.btnpadtop.relative.open > ul "
	 * + "> li:nth-child(4) > a",driver,logger); return element; }
	 */
	 
	
	public int getReschedulefromDropdownNum(WebDriver driver,ExtentTest logger) {
		List<WebElement> myElements = driver.findElements(By.xpath("//a[text()='Reschedule']"));

		int num= myElements.size();
 		return num;
	}
	
	
	  public WebElement getReschedulefromDropdown(int num, WebDriver driver,ExtentTest logger) 
	  { WebElement element =
	  Web_GeneralFunctions.findElementbyXPath("(//a[text()='Reschedule'])["+num+"]", driver, logger); 
	  return element; }
	 
	
	
	// below old Xpath is for cancel link in the drop down 
	  public WebElement getButtonfromDropdown(int m,int n, WebDriver
	  driver,ExtentTest logger) { WebElement element = Web_GeneralFunctions.
	  findElementbySelector("#appointment-container > div.rightbar > ul > li:nth-child("
	  +m+
	  ") > div.muiltiple > div.pull-left.padtop10.col-lg-7 > div.pull-left.padleftright.text-center.btnpadtop.relative > ul "
	  + "> li:nth-child("+n+") > a",driver,logger); return element; }
	  
	  
	 
	public int getCancelfromDropdownNum(WebDriver driver,ExtentTest logger) {
		List<WebElement> myElements = driver.findElements(By.xpath("//a[@class='cursorPointer'][contains(text(),'Cancel')]"));

		int num= myElements.size();
 		return num;
	}
	
	// below new Xpath is for cancel link in the drop down 

	  public WebElement getCancelfromDropdown(int num, WebDriver driver,ExtentTest logger) 
	  {
		  WebElement element = Web_GeneralFunctions.findElementbyXPath("(//a[@class='cursorPointer'][contains(text(),'Cancel')])["+num+"]", driver, logger); 
	  return element; 
	  }
	 
	
	
	/*public WebElement getCancelfromDropdown(int n, WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#appointment-container > div.rightbar > ul > li:nth-child("+n+") > div.muiltiple > div.pull-left.padtop10.col-lg-7 > div.pull-left.padleftright.text-center.btnpadtop.relative.open > ul > li:nth-child(5) > a",driver,logger);
		return element;
	}*/
	
		// below new Xpath is for Check In link in the drop down 

	  
	  public int getCheckInfromDropdownNum(WebDriver driver,ExtentTest logger) {
			List<WebElement> myElements = driver.findElements(By.xpath("//a[@class='cursorPointer'][text()='Check In']"));

			int num= myElements.size();
	 		return num;
		}
		

		  public WebElement getCheckInfromDropdown(int num, WebDriver driver,ExtentTest logger) 
		  {
			  WebElement element = Web_GeneralFunctions.findElementbyXPath("(//a[@class='cursorPointer'][text()='Check In'])["+num+"] ", driver, logger); 
		  return element; 
		  }
		  
		  
		  
		  
// below new Xpath is for Pre Consultation link in the drop down 

		  
		  public int getPreConsultationfromDropdownNum(WebDriver driver,ExtentTest logger) {
				List<WebElement> myElements = driver.findElements(By.xpath("//a[@class='cursorPointer'][text()='Pre Consultation']"));

				int num= myElements.size();
		 		return num;
			}
			

			  public WebElement getPreConsultationfromDropdown(int num, WebDriver driver,ExtentTest logger) 
			  {
				  WebElement element = Web_GeneralFunctions.findElementbyXPath("(//a[@class='cursorPointer'][text()='Pre Consultation'])["+num+"]", driver, logger); 
			  return element; 
			  }
			   
		// below new Xpath is for Pre Consulting link in the drop down 

		  
		  public int getPreConsultingfromDropdownNum(WebDriver driver,ExtentTest logger) {
				List<WebElement> myElements = driver.findElements(By.xpath("//a[@class='cursorPointer'][text()='Pre Consulting']"));

				int num= myElements.size();
		 		return num;
			}
			

			  public WebElement getPreConsultingfromDropdown(int num, WebDriver driver,ExtentTest logger) 
			  {
				  WebElement element = Web_GeneralFunctions.findElementbyXPath("(//a[@class='cursorPointer'][text()='Pre Consulting'])["+num+"]", driver, logger); 
			  return element; 
			  }
			   
		 
			// below new Xpath is for Pre Consulted link in the drop down 

			  
			  public int getPreConsultedfromDropdownNum(WebDriver driver,ExtentTest logger) {
					List<WebElement> myElements = driver.findElements(By.xpath("//a[@class='cursorPointer'][text()='Pre Consulted']"));

					int num= myElements.size();
			 		return num;
				}
				

				  public WebElement getPreConsultedfromDropdown(int num, WebDriver driver,ExtentTest logger) 
				  {
					  WebElement element = Web_GeneralFunctions.findElementbyXPath("(//a[@class='cursorPointer'][text()='Pre Consulted'])["+num+"]", driver, logger); 
				  return element; 
				  }
		  
				// below new Xpath is for Consult link in the drop down 

				  
				  public int getConsultfromDropdownNum(WebDriver driver,ExtentTest logger) {
						List<WebElement> myElements = driver.findElements(By.xpath("//a[@class='cursorPointer'][text()='Consult' or text()='  Consult  ']"));

						int num= myElements.size();
				 		return num;
					}
					

					  public WebElement getConsultfromDropdown(int num, WebDriver driver,ExtentTest logger) 
					  {
						  WebElement element = Web_GeneralFunctions.findElementbyXPath("(//a[@class='cursorPointer'][text()='Consult' or text()='  Consult  '])["+num+"]", driver, logger); 
					  return element; 
					  }
			  			  
					// below new Xpath is for Checked Out link in the drop down
						public int getCheckedOutfromDropdownNum(WebDriver driver, ExtentTest logger) {
							List<WebElement> myElements = driver.findElements(By.xpath("//a[@class='cursorPointer'][text()='Checked Out']"));
							int num = myElements.size();
							return num;
						}
						public WebElement getCheckedOutfromDropdown(int num, WebDriver driver, ExtentTest logger) {
							WebElement element = Web_GeneralFunctions
									.findElementbyXPath("(//a[@class='cursorPointer'][text()='Checked Out'])[" + num + "]", driver, logger);
							return element;
						}
					// below new Xpath is for Consulting link in the drop down 

					  
					  public int getConsultingfromDropdownNum(WebDriver driver,ExtentTest logger) {
							List<WebElement> myElements = driver.findElements(By.xpath("//a[@class='cursorPointer'][text()='Consulting']"));

							int num= myElements.size();
					 		return num;
						}
						

						  public WebElement getConsultingfromDropdown(int num, WebDriver driver,ExtentTest logger) 
						  {
							  WebElement element = Web_GeneralFunctions.findElementbyXPath("(//a[@class='cursorPointer'][text()='Consulting'])["+num+"]", driver, logger); 
						  return element; 
						  }		  
	public WebElement getTimingsfromRescheduleAlert(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#rescheduleTimings",driver,logger);
		return element;
	}
	
	public WebElement setTimingsinRescheduleAlert(int n, WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#rescheduleTimings > option:nth-child("+n+")",driver,logger);
		return element;
	}
	
	public WebElement getOKBtnfromRescheduleAlert(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#rescheduleBtn",driver,logger);
		return element;
	}
	
	
	  public WebElement getReasondropdownfromCancelAlert(WebDriver
	  driver,ExtentTest logger) { WebElement element =
	  Web_GeneralFunctions.findElementbySelector("#reasoncancel",driver,logger);
	  return element; }

	public WebElement setReasoninCancelAlert(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#reasoncancel > option:nth-child(2)",driver,logger);
		return element;
	}
	
	
	public WebElement getSubmitBtnfromCancelAlert(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#cancelAppointmentPopup > div:nth-child(9) > div > div > a:nth-child(1)",driver,logger);
		return element;
	}
	
	
	public WebElement getAppointmentsPage(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#appointmentToggle",driver,logger);
		return element;
	}
	
	
	public WebElement getBlockSlotIcon(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#blockcalbtn > img",driver,logger);
		return element;
	}
	
	
	public WebElement setStartTime(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#fromDateAndTime",driver,logger);
		return element;
	}
	
	
	public WebElement setEndTime(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#toDateAndTime",driver,logger);
		return element;
	}
	
	
	public WebElement getBlockReason(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#reasonBlocks",driver,logger);
		return element;
	}
	
	
	public WebElement setBlockReason(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#reasonBlocks > option:nth-child(3)",driver,logger);
		return element;
	}
	
	public WebElement getBlockSlotSubmitReason(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#btnBlockCalender",driver,logger);
		return element;
	}
	
	
	public WebElement getPatientRegisterBtn(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#topB > div.col-xs-6.col-sm-6.col-md-6.col-lg-6 > div > div.add > a > img", driver, logger);
		return element;
	}
	
	public WebElement getFirstAvailableConsumerStartConsultation(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#patientsearchresult > li:nth-child(1) > div.topspace.pull-right.greentext > a:nth-child(2)",driver,logger);
		return element;
	}
	
	public WebElement getFirstCustomerUHID(WebDriver driver,ExtentTest logger)
	{
		
		WebElement element = Web_GeneralFunctions.findElementbySelector("#patientsearchresult > li:nth-child(1) > div.pull-left.padleftright>div[class*='appointment']",driver,logger);
		return element;
	}
	
	public boolean checkTimings(WebDriver driver,ExtentTest logger) {
		try {
			Web_GeneralFunctions.findElementbySelector("#pageContent > div.container-fluid.text-center.innerpagepad > strong",driver,logger).getText();
		}catch(Exception e) {
			return false;
		}
		return true;
		
	}
	
	public WebElement getDoctorWithTimings(int i, WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbySelector("#clinicUserId > option:nth-child("+i+")",driver,logger);
		return element;
	}


	public WebElement getListfromDropdownValue(String slotid,int n, WebDriver driver,ExtentTest logger) {
		
	String xpath="//div[@id='bookslot"+slotid+"']/ancestor::li//div[@class='muiltiple']//div[@class='pull-left padtop10 col-lg-7']//div[@class='pull-left padleftright text-center btnpadtop relative open']/ul/li["+n+"]/a";
		WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
		return element;
	}
	public WebElement getCheckInButton(WebDriver driver,ExtentTest logger) {
			WebElement element = Web_GeneralFunctions.findElementbyXPath("//button[contains(.,'Check In')]", driver, logger);
			return element;
		}
	public WebElement getCheckoutButtonInConsultPage(WebDriver driver,ExtentTest logger,String checkout) {
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//button[@class='btn uploadbtn btn-lg' and contains(.,'"+checkout+"')]", driver, logger);
		return element;
	}
	public WebElement getCheckoutYesButtonInConsultPage(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//button[@class='confirm']", driver, logger);
		return element;
	}
	public WebElement getCheckoutCloseButtonInConsultPage(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//a[@id='checkoutClose']", driver, logger);
		return element;
	}
	public WebElement getStartFollowUpButtonInConsultPage(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//div[@class='col-md-12' ]/a[contains(.,'Start follow up consultation')]", driver, logger);
		return element;
	}
	public WebElement getShareToFollowUpConsultPage(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//button[@id='softCheckout' and contains(.,' Share to follow up ')]", driver, logger);
		return element;
	}
	public WebElement getSavePreConsultPage(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//a[@id='vitalbtn']", driver, logger);
		return element;
	}
	public WebElement getAppointmentStatus(String slotid,WebDriver driver,ExtentTest logger) {
		
		String xpath="//div[@id='bookslot"+slotid+"']/ancestor::li//div[@class='muiltiple']//div[@class='graytext btnpadtop pull-left padleft10']/div";
			WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
			return element;
		}
	
	public WebElement getNotePreConsultPage(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//*[@id='txtparameterComment']", driver, logger);
		return element;
	}
	public WebElement getRescheduleDropdown(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//select[@id='rescheduleTimings']", driver, logger);
		return element;
	}
	public List<WebElement> getRescheduleDropdownOptionList(WebDriver driver,ExtentTest logger) 
	{
		return Web_GeneralFunctions.listOfElementsbyXpath("//select[@id='rescheduleTimings']/option", driver, logger);	
	}
	public WebElement getRescheduleAppointmetOk(WebDriver driver,ExtentTest logger) {
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//button[@class='btn  greenbtn' and contains(.,'Ok')]", driver, logger);
		return element;
	}
public WebElement getAppointmentPaymentMode(String slotid,WebDriver driver,ExtentTest logger) {
		Web_GeneralFunctions.findElementbyXPath("//div[@id='bookslot"+slotid+"']/ancestor::li//div[@class='muiltiple']//div[@class='pull-right  text-center']", driver, logger).isDisplayed();
		WebElement element = Web_GeneralFunctions.findElementbyXPath("//div[@id='bookslot"+slotid+"']/ancestor::li//div[@class='muiltiple']//div[@class='pull-right  text-center']/div/div", driver, logger);
			return element;
		}
	
public WebElement getAppointmentDropdownButton(String slotid,WebDriver driver,ExtentTest logger) {
String xpath="//div[@id='bookslot"+slotid+"']/ancestor::li//div[@class='muiltiple']//div[@class='pull-left padtop10 col-lg-7']//div[@class='pull-left padleftright text-center btnpadtop relative']/a/img";
WebElement element = Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger);
return element;
}

}
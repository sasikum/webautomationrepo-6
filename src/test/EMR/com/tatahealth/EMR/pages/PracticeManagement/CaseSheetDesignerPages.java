package com.tatahealth.EMR.pages.PracticeManagement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import org.apache.commons.lang.math.RandomUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.tatahealth.GeneralFunctions.Web_GeneralFunctions;

public class CaseSheetDesignerPages {

	
	public WebElement getMoveToCaseSheetDesigner(WebDriver driver,ExtentTest logger){
		
		String xpath = "//a[@id='examinationCaseSheetDesignerLia']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getSelectCaseSheetDesignerDropDown(WebDriver driver,ExtentTest logger){
		

		String xpath = "//span[@class='multiselect-selected-text']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement selectOneElement(WebDriver driver,ExtentTest logger){
		String xpath = "//div[contains(@class,'btn-group open')]//ul[contains(@class,'multiselect-container dropdown-menu')]/li";
		List<WebElement> li = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		int size = li.size();
		
		
		int index = RandomUtils.nextInt(size);
		
		return(li.get(index));
		
	}
	
	
	public WebElement getSelectButton(WebDriver driver,ExtentTest logger){
		
		String xpath = "//button[contains(text(),'Select')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	public WebElement getPreviewCaseSheetButton(WebDriver driver,ExtentTest logger){
		
		String xpath = "//button[contains(text(),'Preview Case Sheet')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getSaveCaseSheetButton(WebDriver driver,ExtentTest logger){
		
		String xpath = "//button[contains(text(),'Save Case Sheet')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
		
	}
	
	public WebElement getPreviewCloseCaseSheetButton(WebDriver driver,ExtentTest logger){
		
		String xpath = "//div[@id='caseSheetPreview']//button[@class='btn btn-default'][contains(text(),'Close')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
		 
	}	
	
	
	public WebElement getSaveCustomCaseSheetButton(WebDriver driver,ExtentTest logger){
		
		String xpath = "//button[@id='saveExaminationTemplate']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getExaminationTemplateName(WebDriver driver,ExtentTest logger){
		
		String xpath = "//input[@id='examinationTemplateName']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getPreviewCloseByXCaseSheetButton(WebDriver driver,ExtentTest logger){
		
		String xpath = "//div[@id='caseSheetPreview']//span[contains(text(),'×')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}	
	
	public WebElement get1stSection(WebDriver driver,ExtentTest logger){
		
		String xpath = "//div[@id='examinationCasesheetSectionDiv']/form/div/div[1]/span/div/label/input";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}	
	
	public WebElement getSweetAlertPopUp(WebDriver driver,ExtentTest logger){
		
		String xpath = "//div[@class='sweet-alert showSweetAlert visible']/p";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}	
	
	public WebElement getNoButtonInPopUp(WebDriver driver,ExtentTest logger){
		
		String xpath = "//button[@class='cancel']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}	
	
	public WebElement getYesButtonInPopUp(WebDriver driver,ExtentTest logger){
		
		String xpath = "//button[@class='confirm']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}	
	
	public WebElement getMessage(WebDriver driver,ExtentTest logger){
		
		String xpath = "//div[contains(@class,'animated fadeInDown')]//div[@class='toast-message']";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	public WebElement getClinicalNotesElement(WebDriver driver,ExtentTest logger){
		
		String xpath = "//label[contains(text(),'Clinical Notes')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	public WebElement getHyperTensionElement(WebDriver driver,ExtentTest logger){
		
		String xpath = "//label[contains(text(),' Hypertension Case Sheet')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public WebElement getSavedCaseSheetName(WebDriver driver,String templateName,ExtentTest logger) {
		String xpath = "//label[contains(text(),'"+templateName+"')]";
		
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	public WebElement getSavePopUpCloseButton(WebDriver driver,ExtentTest logger){
		
		String xpath = "//div[@id='savecasesheet']//button[@class='btn btn-default'][contains(text(),'Close')]";
		return(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public boolean getCaseSheetHeadingDisplayStatus(WebDriver driver) throws InterruptedException{
		
		String xpath = "//h4[contains(text(),'Casesheet Designer')]";
		return(Web_GeneralFunctions.isDisplayed(xpath,driver));	
		 
	}
	
	public boolean getCaseSheetPreviewHeading(WebDriver driver) throws InterruptedException{
		
		String xpath = "//h4[contains(text(),' Case Sheet Preview ')]";
		return(Web_GeneralFunctions.isDisplayed(xpath,driver));	
		
	}
	
	
	
	public List<String> getAllExaminationNamesAsList(WebDriver driver,String xpath ,ExtentTest logger) {

		List<String> caseSheetNames =new ArrayList<>();
		List<WebElement> subcaseSheets = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		for(WebElement element : subcaseSheets) {
			
			caseSheetNames.add(element.getText().split(":")[0].trim());
		}
		return caseSheetNames;
		
	}
	
	public String getCaseSheetExaminationsXpath() {
		return("//div[@id='accordion-practicemanagement']//div//child::a");
		
	}
	
	public String getPreviewCaseSheetExaminationsXpath() {
		return("//div[@id='caseSheetPreviewBody']//child::a");
		
	}
	
	public List<WebElement> getAllExaminationCheckBoxesAsList(WebDriver driver,ExtentTest logger){
		String xpath = "//div[@id='accordion-practicemanagement']//child::input";
		return(Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
	}
	
	public boolean getCaseSheetMatchStatus(List<String> casesheet1,List<String> casesheet2) {
		Collections.sort(casesheet1);
		Collections.sort(casesheet2);
		return(casesheet1.equals(casesheet2));
		
		
	}
	
	public String getActiveCaseSheetNameFromDropdown(WebDriver driver,ExtentTest logger) throws Exception {
		String xpath = "//span[@class='multiselect-selected-text']";
		String caseSheetName = Web_GeneralFunctions.getText(Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger),"get Selected caseSheet Name", driver, logger);
		return caseSheetName;
	}
	
	public WebElement getCaseSheetSelectbuttonInConsultationPage(WebDriver driver,ExtentTest logger) {
		String xpath ="//input[@value='Select']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	
	public List<WebElement> getAllCaseSheetsFromDropDown(WebDriver driver,ExtentTest logger){
		String xpath ="//ul[@class='multiselect-container dropdown-menu']//child::input";
		return(Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger));
		
	}
	public WebElement getCaseSheetElementInConsultationPage(WebDriver driver,ExtentTest logger) {
		String xpath = "//li[@data-input='Examination']";
		return (Web_GeneralFunctions.findElementbyXPath(xpath, driver, logger));
	}
	
	public List<String> getSelectedCaseSheetExaminationNamesInConsultationPage(WebDriver driver,ExtentTest logger) throws Exception{
		List<String> ExaminationsList = new ArrayList<>();
		String xpath = "//div[@id='accordion-practicemanagement']";
		List<WebElement> listOfCaseSheets = Web_GeneralFunctions.findElementsbyXpath(xpath, driver, logger);
		String xpath2 = "(//div[@id='accordion-practicemanagement'])["+listOfCaseSheets.size()+"]"+"//child::h4//a";
		List<WebElement> ExaminationsNamesList = Web_GeneralFunctions.findElementsbyXpath(xpath2, driver, logger);
		for(WebElement e : ExaminationsNamesList) {
			ExaminationsList.add(Web_GeneralFunctions.getText(e, "getting text", driver, logger).trim());
		}
		return ExaminationsList;
	}
	
	
	
}

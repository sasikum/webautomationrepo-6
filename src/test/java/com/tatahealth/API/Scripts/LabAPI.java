package com.tatahealth.API.Scripts;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.tatahealth.API.libraries.SheetsAPI;
import com.tatahealth.ReusableModules.Web_Testbase;


public class LabAPI {
	
		
	private List<NameValuePair> param;
	private List<NameValuePair> header;


	public JSONObject AssignCollectionAgent(String orderId) throws Exception {
		
		header = new ArrayList<NameValuePair>();
		header.add(new BasicNameValuePair("UserId","username"));
		header.add(new BasicNameValuePair("access_token","password"));
		
		Date date = new Date();
		String epochTime = Long.toString(date.getTime());
		
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("agentAssignedMobileNumber","9177536803"));
		param.add(new BasicNameValuePair("agentAssignedPersonName","Agent K"));
		param.add(new BasicNameValuePair("sealNbr",orderId));
		param.add(new BasicNameValuePair("status","Load Assigned"));
		param.add(new BasicNameValuePair("statusChangeDate",epochTime));
		param.add(new BasicNameValuePair("statusCode","7001"));
		
		String URL = SheetsAPI.getDataConfig(Web_Testbase.input+"!B14");
		System.out.println(URL);
		GeneralAPIFunctions API = new GeneralAPIFunctions();
		JSONObject resp = API.PostHeadersWithRequest(URL, param, header);
		return resp;
		
	}
	
	
	public JSONObject CollectionAgentReachedHome(String orderId) throws Exception {
		
		header = new ArrayList<NameValuePair>();
		header.add(new BasicNameValuePair("UserId","username"));
		header.add(new BasicNameValuePair("access_token","password"));
		
		Date date = new Date();
		String epochTime = Long.toString(date.getTime());
		
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("agentAssignedMobileNumber","9177536803"));
		param.add(new BasicNameValuePair("agentAssignedPersonName","Agent K"));
		param.add(new BasicNameValuePair("sealNbr",orderId));
		param.add(new BasicNameValuePair("status","STOP_ARRIVAL"));
		param.add(new BasicNameValuePair("statusChangeDate",epochTime));
		param.add(new BasicNameValuePair("statusCode","7007"));
		
		String URL = SheetsAPI.getDataConfig(Web_Testbase.input+"!B14");
		GeneralAPIFunctions API = new GeneralAPIFunctions();
		JSONObject resp = API.PostHeadersWithRequest(URL, param, header);
		return resp;
		
	}
	
	public JSONObject CollectionCompleted(String orderId) throws Exception {
		
		header = new ArrayList<NameValuePair>();
		header.add(new BasicNameValuePair("UserId","username"));
		header.add(new BasicNameValuePair("access_token","password"));
		
		Date date = new Date();
		String epochTime = Long.toString(date.getTime());
		
		param = new ArrayList<NameValuePair>();
		param.add(new BasicNameValuePair("agentAssignedMobileNumber","9177536803"));
		param.add(new BasicNameValuePair("agentAssignedPersonName","Agent K"));
		param.add(new BasicNameValuePair("sealNbr",orderId));
		param.add(new BasicNameValuePair("status","STOP_DELIVERY"));
		param.add(new BasicNameValuePair("statusChangeDate",epochTime));
		param.add(new BasicNameValuePair("statusCode","7009"));
		
		String URL = SheetsAPI.getDataConfig(Web_Testbase.input+"!B14");
		GeneralAPIFunctions API = new GeneralAPIFunctions();
		JSONObject resp = API.PostHeadersWithRequest(URL, param, header);
		return resp;
		
	}
	
	
	public JSONObject readMail(String URL) throws Exception {
		
		//String URL = "http://52.66.123.140:9590/EmailUpload/Email/readLabResultsEmailsCentral";
		GeneralAPIFunctions API = new GeneralAPIFunctions();
		JSONObject resp = API.getRequest(URL);
		return resp;
		
	}
	
}
